# Hi! I'm Triston Armstrong
A Software developer from the USA

- Working at [VentraHealth](https://ventrahealth.com)

## Skills
- Typescript, Javascript, Python, Rust
- React, NextJs, Vanilla
- Css, Tailwind, MUI
- Havent touched a DB in a fat minute

## Contact
- [Portfolio](https://tristonarmstrong.com)
- [X](https://x.com/triston_armstr)
- [Fosstodon](https://fosstodon.org/@TristonArmstrong)
- [Github](https://github.com/tristonarmstrong)
